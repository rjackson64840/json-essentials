/*
The MIT License (MIT)

Copyright (c) 2019-2022 Russ Jackson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package org.hawksoft.json;

import java.util.Map;

/**
 * Created at 12:16 AM on 2019-12-08
 *
 * @author Russ Jackson
 */
public class JsonWriter {

    private static final String NEWLINE = System.lineSeparator();
    private static final int MAX_INDENT = 10;

    private String _indent;

    private boolean _shouldIndent;

    private int _level;

    private final StringBuilder _sb = new StringBuilder();

    public JsonWriter(int indent) {
        initIndentation(indent);
    } // constructor

    public String write(JsonObject json) {
        if (null != json) {
            clear();
            doWriteJsonArray(json);
        }
        return _sb.toString();
    } // write()

    private void clear() {
        _sb.setLength(0);
    } // clear()

    private void append(char ch) {
        _sb.append(ch);
    } // append()

    private void append(String string) {
        _sb.append(string);
    } // append()

    private void beginArray() {
        append('[');
    } // beginArray()

    private void beginObject() {
        _level++;
        append('{');
    } // beginObject()

    private <T> void doWrite(JsonArray<T> json) {
        beginArray();
        boolean firstValue = true;
        for (T value : json.values()) {
            writeArrayValue(value, firstValue);
            firstValue = false;
        }
        endArray();
    }

    private void doWriteJsonArray(JsonObject json) {
        beginObject();
        boolean firstAttribute = true;
        for (Map.Entry<String, Object> entry : json.entrySet()) {
            writeAttribute(entry.getKey(), entry.getValue(), firstAttribute);
            firstAttribute = false;
        }
        endObject();
    } // doWriteObject()

    private void endArray() {
        append(']');
    } // endArray()

    private void endObject() {
        _level--;
        newline();
        indent();
        append('}');
    } // endObject()

    private void indent() {
        if (_shouldIndent) {
            for (int i = 0; i < _level; i++) {
                append(_indent);
            }
        }
    } // indent()

    private void initIndentation(int indentation) {
        indentation = Math.min(MAX_INDENT, Math.max(0, indentation));
        _shouldIndent = indentation > 0;
        StringBuilder padding = new StringBuilder(indentation);
        for (int i = 0; i < indentation; i++) {
            padding.append(' ');
        }
        _indent = padding.toString();
    } // initIndentation()

    private void keyValueSeparator() {
        makePretty(':');
    } // separateKeyValue()

    private void makePretty(char ch) {
        append(ch);
        if (_shouldIndent) {
            append(' ');
        }
    } // makePretty()

    private void newline() {
        if (_shouldIndent) {
            append(NEWLINE);
        }
    } // newline()

    private void quote(String value) {
        append('"');
        append(value);
        append('"');
    } // quote()

    public <T> String write(JsonArray<T> json) {
        if (null != json) {
            clear();
            doWrite(json);
        }
        return _sb.toString();
    } // write()

    private <T> void writeArrayValue(T value, boolean firstValue) {
        if (!firstValue) {
            makePretty(',');
        }
        writeValue(value);
    } // writeArrayValue()

    private <T> void writeAttribute(String key, T value, boolean firstAttribute) {
        if (!firstAttribute) {
            append(',');
        }
        newline();
        indent();
        quote(key);
        keyValueSeparator();
        writeValue(value);
    } // writeAttribute()

    private <T> void writeValue(T value) {
        if (value instanceof String) {
            quote((String)value);
        } else if (value instanceof JsonObject) {
            doWriteJsonArray((JsonObject)value);
        } else if (value instanceof JsonArray) {
            doWrite((JsonArray<?>)value);
        } else {
            _sb.append(value.toString());
        }
    } // writeValue()

} // class JsonWriter
