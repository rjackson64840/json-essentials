/*
The MIT License (MIT)

Copyright (c) 2019-2022 Russ Jackson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/
package org.hawksoft.json;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created at 7:07 PM on 2019-12-06
 *
 * @author Russ Jackson
 */
public class JsonArray<T> {

    private final List<T> _data = new ArrayList<>();

    public JsonArray() {
    } // constructor

    public JsonArray(String json) {
        StringIndexer indexer = new StringIndexer(json.trim());
        captureArray(indexer);
        indexer.expectEnd();
    } // constructor

    JsonArray(StringIndexer indexer) {
        captureArray(indexer);
    }

    public JsonArray<T> add(T value) {
        if (null != value) {
            _data.add(value);
        }
        return this;
    } // add()

    public boolean contains(T value) {
        return null != value && _data.contains(value);
    } // contains()

    public int length() { return _data.size(); }

    public String toString(int indentation) {
        return new JsonWriter(indentation).write(this);
    } // toString()

    public String toString() {
        return new JsonWriter(0).write(this);
    }

    public List<T> values() {
        return Collections.unmodifiableList(_data);
    } // values()

    /**
     * '[' -> array -> expect(']', value + repeat(comma + value) + ']' )
     */
    private void captureArray(StringIndexer indexer) {
        indexer.expect('[');
        if (indexer.isNotNext(']')) {
            indexer.back();
            do {
                _data.add(JsonHelper.captureValue(indexer));
            } while (!indexer.isAt(']') && indexer.isAtOrNext(','));
            indexer.expect(']');
        }
    } // captureArray()

} // class JsonArray
