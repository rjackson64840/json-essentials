/*
The MIT License (MIT)

Copyright (c) 2019-2022 Russ Jackson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package org.hawksoft.json;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Created at 8:30 PM on 2019-12-04
 *
 * @author Russ Jackson
 */
public class JsonObject {

    private final Map<String, Object> _data = new LinkedHashMap<>();

    Set<Map.Entry<String, Object>> entrySet() { return _data.entrySet(); }

    public JsonObject() {
        // default constructor
    } // constructor

    public JsonObject(String json) {
        parse(json.trim());
    }

    public JsonObject(StringIndexer indexer) {
        captureObject(indexer);
    }

    public JsonObject(String key, Object value) {
        put(key, value);
    }

    public JsonObject createObject(String key) {
        JsonObject result = null;
        if (isNotBlank(key)) {
            result = new JsonObject();
            put(key, result);
        }
        return result;
    } // createObject()

    public <T> JsonArray<T> createArray(String key) {
        JsonArray<T> result = null;
        if (isNotBlank(key)) {
            result = new JsonArray<>();
            put(key, result);
        }
        return result;
    } // createArray()

    public <T> JsonObject append(String key, T value) {
        if (isNotBlank(key) && null != value) {
            JsonArray<T> array = get(key);
            if (null == array) {
                array = new JsonArray<>();
                put(key, array);
            }
            array.add(value);
        }
        return this;
    } // append()

    public <T> T get(String key) {
        return get(key, null);
    } // get()

    @SuppressWarnings("unchecked")
    public <T> T get(String key, T defaultValue) {
        T result = defaultValue;
        if (has(key)) {
            try {
                result = (T)_data.get(key);
            } catch (ClassCastException e) {
                if ("java.lang.Integer cannot be cast to java.lang.Double".equals(e.getMessage())) {
                    Integer intValue = (Integer)_data.get(key);
                    result =  (T)(Double.valueOf(intValue.doubleValue()));
                }
            }
        }
        return result;
    } // get()

    public Double getDouble(String key) {
        return getDouble(key, null);
    }

    public Double getDouble(String key, Double defaultValue) {
        Double result = defaultValue;

        if (has(key)) {
            Object value = _data.get(key);
            if (value instanceof Double) {
                result = (Double)value;
            } else if (value instanceof Integer) {
                result = ((Integer) value).doubleValue();
            } else if (value instanceof String) {
                result = Double.valueOf((String) value);
            } else if (value instanceof Long) {
                result = ((Long) value).doubleValue();
            } else if (value instanceof BigDecimal) {
                result = ((BigDecimal) value).doubleValue();
            } else {
                throw new IllegalStateException(value.getClass().getSimpleName());
            }
        }

        return result;
    } // getDouble()

    public String optGet(String key) {
        return get(key, "");
    } // optGet()

    public LocalDate getDate(String key) {
        String value = get(key);
        return null != value ? LocalDate.parse(value, DateTimeFormatter.ISO_DATE) : null;
    } // getDate()

    public LocalDateTime getDateTime(String key) {
        String value = get(key);
        return null != value ? LocalDateTime.parse(value, DateTimeFormatter.ISO_DATE_TIME) : null;
    } // getDateTime()

    public boolean has(String key) {
        return isNotBlank(key) && _data.containsKey(key);
    } // has()

    public <T> JsonArray<T> newChildArray(String name) {
        if (isBlank(name)) {
            return null;
        }
        JsonArray<T> childArray = new JsonArray<>();
        _data.put(name, childArray);
        return childArray;
    } // startChildArray()

    public JsonObject newChildObject(String name) {
        if (isBlank(name)) {
            return null;
        }
        JsonObject childObject = new JsonObject();
        _data.put(name, childObject);
        return childObject;
    } // startChildObject()

    public JsonObject put(String key, JsonObject value) {
        return put(key, (Object)value);
    } // put()

    public <T> JsonObject put(String key, JsonArray<T> value) {
        return put(key, (Object)value);
    } // put()

    public JsonObject put(String key, Double value) {
        return put(key, (Object)value);
    } // put()

    public JsonObject put(String key, Long value) {
        return put(key, (Object)value);
    } // put()

    public JsonObject put(String key, Integer value) {
        return put(key, (Object)value);
    } // put()

    public JsonObject put(String key, String value) {
        return put(key, (Object)value);
    } // put()

    public JsonObject put(String key, LocalDate date) {
        if (null != date) {
            return put(key, date.format(DateTimeFormatter.ISO_DATE));
        } else {
            return this;
        }
    } // put()

    public JsonObject put(String key, LocalDateTime date) {
        if (null != date) {
            return put(key, date.format(DateTimeFormatter.ISO_DATE_TIME));
        } else {
            return this;
        }
    } // put()

    public final JsonObject put(String key, Object value) {
        if (isNotBlank(key) && null != value) {
            _data.put(key, value);
        }
        return this;
    } // put()

    public String toString(int indentation) {
        return new JsonWriter(indentation).write(this);
    } // toString()

    public String toString() {
        return new JsonWriter(0).write(this);
    }

    /**
     *   attr -> expect(key) + expect(':') + expect(value)
     */
    private void captureAttribute(StringIndexer indexer) {
        String key = captureKey(indexer);
        indexer.expect(':');
        _data.put(key, JsonHelper.captureValue(indexer));
    } // captureAttribute()

    /**
     * " -> key -> text = seek(")
     */
    private static String captureKey(StringIndexer indexer) {
        indexer.nextIf(',');
        indexer.expect('"');
        return indexer.pullText();
    } // captureKey()

    /**
     * '{' -> obj -> expect( '}', attr + repeat(comma + attr) + '}' ) )
     */
    private void captureObject(StringIndexer indexer) {
        indexer.expect('{');
        if (indexer.isNotNext('}')) {
            do {
                captureAttribute(indexer);
            } while (!indexer.isAt('}') && indexer.isAtOrNext(','));
            indexer.expect('}');
            indexer.next();
        }
    } // captureObject()

    private void parse(String json) {
        StringIndexer indexer = new StringIndexer(json.trim());
        captureObject(indexer);
        indexer.expectEnd();
    }

} // class JsonObject
