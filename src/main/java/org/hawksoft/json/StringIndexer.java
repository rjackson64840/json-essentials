/*
The MIT License (MIT)

Copyright (c) 2019-2022 Russ Jackson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package org.hawksoft.json;

/**
 * Created at 9:51 AM on 2019-12-05
 *
 * @author Russ Jackson
 */
class StringIndexer {

    private static final int ERROR_CAPTURE_CHARS = 20;

    // source string being indexed
    private final String _source;

    private final int _length;

    private final int _lastIndex;

    private int _currentIndex;

    private static final String WHITESPACE = "\r\n\t ";

    private static final String WORD_DELIMITER = WHITESPACE + ",]}";

    StringIndexer(String source) {
        _source = source;
        _length = _source.length();
        _lastIndex = _length - 1;
    } // constructor

    // go back one character
    void back() {
        if (_currentIndex > 0) {
            _currentIndex--;
        }
    } // back()

    // advance past whitespace to pick up next character
    char next() {
        _currentIndex++;
        while (isWhitespace()) {
            _currentIndex++;
        }
        return hasMore() ? ch() : 0;
    } // next()

    boolean isNotNext(char ch) {
        return ch != next();
    } // isNext()

    boolean isAtOrNext(char ch) {
        return ch == ch() || ch == next();
    } // isAtOrNext()

    // go to the next character if the current character is equal to the one provided
    void nextIf(char ch) {
        if (ch == ch()) {
            next();
        }
    } // nextIf()

    // is the current character equal to the one provided?
    boolean isAt(char ch) {
        return ch == ch();
    } // isAt()

    // the current character is expected to match the given one
    // throws an exception if not
    void expect(char ch) {
        if (ch != ch()) {
            throw new InvalidJsonException(String.format(
                "Expected '%s' at position %d : %s⏵%s⏴%s",
                ch,
                _currentIndex,
                _source.substring(Math.max(0, _currentIndex - ERROR_CAPTURE_CHARS), _currentIndex),
                _source.charAt(_currentIndex),
                _source.substring(_currentIndex + 1, Math.min(_length, _currentIndex + ERROR_CAPTURE_CHARS))
            ));
        }
    } // expect()

    // expect no more content
    void expectEnd() {
        next();
        if (hasMore()) {
            throw new InvalidJsonException(String.format("Unexpected character(s) at end of JSON : %s⏵%s⏴",
                _source.substring(Math.max(0, _currentIndex - ERROR_CAPTURE_CHARS), _currentIndex),
                _source.substring(_currentIndex)
            ));
        }
    } // expectEnd()

    // pull a string bounded by double quotes
    String pullText() {
        int startingIndex = _currentIndex;
        if (hasMore() && '"' == _source.charAt(startingIndex)) {
            startingIndex++;
        }
        _currentIndex = startingIndex;
        boolean found = '"' == _source.charAt(_currentIndex);
        while (!found) {
            if (_currentIndex + 1 > _lastIndex) {
                throw new InvalidJsonException(String.format(
                        "Unexpected end of JSON string: %s⏴",
                        _source.substring(Math.max(0, _length - ERROR_CAPTURE_CHARS))
                    ));
            }
            found = ch() != '\\' && _source.charAt(_currentIndex + 1) == '"';
            _currentIndex++;
        }
        String result = _source.substring(startingIndex, _currentIndex);
        next();
        return result;
    } // pullText()

    // pull a word delimited by whitespace and special json characters
    String pullWord() {
        int startingIndex = _currentIndex;
        while (hasMore() && WORD_DELIMITER.indexOf(ch()) < 0) {
            _currentIndex++;
        }
        return _source.substring(startingIndex, _currentIndex);
    } // pullWord()

    char ch() {
        return _source.charAt(_currentIndex);
    } // ch()

    private boolean hasMore() {
        return _currentIndex < _length;
    } // hasMore()

    private boolean isWhitespace() {
        return hasMore() && Character.isWhitespace(ch());
    } // isWhitespace()

} // class StringIndexer
