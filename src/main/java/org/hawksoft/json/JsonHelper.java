/*
The MIT License (MIT)

Copyright (c) 2019-2022 Russ Jackson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package org.hawksoft.json;

import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Created at 7:15 PM on 2019-12-06
 *
 * @author Russ Jackson
 */
final class JsonHelper {

    private JsonHelper() {

    }

    /**
     * value -> expect('[' -> array, '{' -> obj, boolean, ", numeric)
     */
    @SuppressWarnings("unchecked")
    static <T> T captureValue(StringIndexer indexer) {
        return switch (indexer.next()) {
            case '[' -> (T)new JsonArray<>(indexer);
            case '{' -> (T)new JsonObject(indexer);
            case '"' -> (T)indexer.pullText();
            default-> (T)wordToObject(indexer.pullWord());
        };
    } // captureValue()

    private static Object wordToObject(String word) {
        Object result;

        if (!isBlank(word)) {
            result = switch (word) {
                case "true" -> Boolean.TRUE;
                case "false" -> Boolean.FALSE;
                default -> wordToNumber(word);
            };
        } else {
            throw new InvalidJsonException(String.format("⏵%s⏴is not a number", word));
        }

        return result;
    } // wordToObject()

    private static Object wordToNumber(String word) {
        Object result;
        if (word.indexOf('.') > -1) {
            try {
                result = Double.valueOf(word);
            } catch (NumberFormatException nfe) {
                throw new InvalidJsonException(
                    String.format("⏵%s⏴is not a valid decimal number", word)
                );
            }
        } else {
            try {
                result = Integer.valueOf(word);
            } catch (NumberFormatException nfe) {
                try {
                    result = Long.valueOf(word);
                } catch (NumberFormatException nfe2) {
                    throw new InvalidJsonException(
                        String.format("⏵%s⏴is not a valid number", word)
                    );
                }
            }
        }
        return result;
    } // wordToNumber()

} // class AbstractJsonElement
