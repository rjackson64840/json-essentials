/*
The MIT License (MIT)

Copyright (c) 2019-2022 Russ Jackson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package org.hawksoft.json

import org.apache.commons.collections4.CollectionUtils
import org.hawksoft.json.InvalidJsonException
import org.hawksoft.json.JsonArray
import org.hawksoft.json.JsonObject
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created at 8:24 PM on 2019-12-06
 *
 * @author Russ Jackson
 */
class JsonArraySpec
        extends Specification {

    @Unroll('checking invalid #badJson')
    def 'test parse invalid json'() {
        when:
        new JsonArray<String>(badJson)

        then:
        thrown InvalidJsonException

        where:
        // don't support hex yet
        badJson << ['{"key":"value"}', 'text', '][', 'no[pe]', '[no]pe', '[]]','[1,2,]', '[0xdefg]', '[0xc0c0babe]', '[1.2.2]']
    }

    @Unroll('checking valid #goodJson')
    def 'test parse valid json'() {
        when:
        def json = new JsonArray(goodJson)

        then:
        json != null

        where:
        goodJson << ['[]', '[{}]', '[  1,2,3,4.56,"78", "9"]']
    }

    def 'test basic array functions'() {
        when:
        def json = new JsonArray<Double>()

        then:
        CollectionUtils.isEmpty(json.values())

        when:
        def value = 1.97
        json.add(value)

        then:
        json.contains(value)
        1 == json.length()

        when:
        List<Double> values = json.values()
        List objects = json.values()

        then:
        CollectionUtils.isNotEmpty(values)
        CollectionUtils.isNotEmpty(objects)

        when:
        json.add(2.25)

        then:
        2 == json.length()
    }

    def 'test complex json'() {
        given:
        def json = '''
[{
  "array" : [{"k1":"v1","k2":2},{"k3":true},{"k4":[5, 6, 7]}],
  "array2" : [ { "k1": "v1" ,"k2" :2} ,{ "k3" : true }  , {"k4":[   5, 6, 7] } ]  ,
  "o2": {
    "k1": {
      "k2": false,
      "k3": 1.5,
      "k4": "no"
    }
    ,"k5" : 0,
    "k6": ["apple", "banana", "orange"]
  }
}]
'''
        when:
        def jo = new JsonArray<JsonObject>(json)
        def out = jo.toString(2)
        println out
        println out.length() + ' characters'
        println '************************************************************'
        out = jo.toString()
        println out
        println out.length() + ' characters'

        then:
        jo != null
    }

    def 'test simpler json'() {
        given:
        def json = '''
[{
  "o2": {
    "k1": {
      "k2": false,
      "k3": 1.5,
      "k4": "no"
    },
    "k5" : 0,
    "k6": ["apple", "banana", "orange"]
  }
}]
'''
        when:
        def ja = new JsonArray<JsonObject>(json)

        then:
        ja != null
        1 == ja.length()
    }
}
