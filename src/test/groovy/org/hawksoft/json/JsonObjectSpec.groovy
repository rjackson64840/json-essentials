/*
The MIT License (MIT)

Copyright (c) 2019-2022 Russ Jackson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package org.hawksoft.json

import org.hawksoft.json.InvalidJsonException
import org.hawksoft.json.JsonArray
import org.hawksoft.json.JsonObject
import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Month
import java.time.format.DateTimeFormatter

import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Created at 11:45 PM on 2019-12-04
 *
 * @author Russ Jackson
 */
class JsonObjectSpec
        extends Specification {

    @Unroll('checking invalid #badJson')
    def 'test parse invalid json'() {
        when:
            new JsonObject(badJson)

        then:
            thrown InvalidJsonException

        where:
            badJson << ['[array]', 'text', '{nope', 'nope}', '}{', 'no{pe}', '{no}pe', '{[]}', '{}}', '{"key":"value}']
    }

    @Unroll('checking valid #goodJson')
    def 'test parse valid json'() {
        when:
        def json = new JsonObject(goodJson)

        then:
        json != null

        where:
        goodJson << ['{}', '{"a":true}', '{ "a" : true }', '{ "bcdef" : 1.7529 }', '{ "ghij" : "seventy six" }',
                     '{ "klmn" : { "en":false,"cnt":2}}',
                     '{ "opq" : { "en":false , "cnt":2 , "arr" : [1, 2, 5] }  } '
        ]
    }

    def 'test append'() {
        given:
        def json = new JsonObject()
        def value = "someValue"
        def value2 = "someOtherValue"

        when:
        json.append("someKey", value)
        JsonArray array = json.get("someKey")

        then:
        array.contains(value)
        !array.contains(value2)

        when:
        json.append("someKey", value2)
        array = json.get("someKey")

        then:
        array.contains(value)
        array.contains(value2)
        2 == array.length()

        when:
        json.append("otherKey", null)
        array = json.get("otherKey")

        then:
        null == array

        when:
        json.append(null, "value3")
        array = json.get(null)

        then:
        null == array
    }

    def 'test init with KV pair'() {
        when:
        def json = new JsonObject("key", "value")

        then:
        "value" == json.get("key")

        when:
        json.put("key", "differentValue")

        then:
        "differentValue" == json.get("key")
    }

    def 'test optGet'() {
        given:
        def json = new JsonObject()

        when:
        String value = json.optGet("missing")

        then:
        value == ""

        when:
        json.put("missing", null)
        value = json.get("missing")

        then:
        value == null

        when:
        value = json.optGet("missing")

        then:
        value == ""
    }

    def 'test complex json'() {
        given:
        def json = '''
{
  "array" : [{"k1":"v1","k2":2},{"k3":true},{"k4":[5, 6, 7]}],
  "array2" : [ { "k1": "v1" ,"k2" :2} ,{ "k3" : true }  , {"k4":[   5, 6, 7] } ]  ,
  "o2": {
    "k1": {
      "k2": false,
      "k3": 1.5,
      "k4": "no"
    },
    "k5" : 0,
    "k6": ["apple", "banana", "orange"]
  }
}
'''
        when:
        def jo = new JsonObject(json)
        def out = jo.toString(2)
        println out
        println out.length() + ' characters'
        println '************************************************************'
        out = jo.toString()
        println out
        println out.length() + ' characters'

        then:
        jo != null
        jo.has("array")
        jo.has("array2")
        jo.has("o2")
        !jo.has("k5")
        !jo.has("k6")
    }

    @Unroll("verify #key @ #value = #expect")
    def 'test get double'() {
        when:
        JsonObject json = new JsonObject()
        json.put(key, value)

        then:
        json.has(key) && expect == json.getDouble(key)

        where:
        key | value | expect
        "doubleAsString" | "1.23" | 1.23
        "doubleAsInt" | (Integer)3 | 3.0
        "doubleAsFloat" | 5.25f | 5.25
        "double" | (Double)7.99 | 7.99
        "object" | (Object)10.33 | 10.33
        "doubleAsLong" | 4L | 4.0
    }

    def 'test create nested object and array'() {
        when:
        JsonObject json = new JsonObject()
        json.createObject("object")
        json.createArray("array")

        then:
        json.get("object") instanceof JsonObject
        json.get("array") instanceof JsonArray
        null == json.createObject("")
        null == json.createArray("")
    }

    def 'test create json object'() {
        when:
        JsonObject json = new JsonObject();
        json.put("", 25);
        json.put("nullInt", (Integer)null)
        json.put("integer", 33)
        json.put("string", "something")
        json.put("double", 1.25d)
        json.put("long", 145L)

        then:
        !json.has("")
        !json.has("nullInt")
        json.has("integer") && 33 == json.get("integer")
        json.has("string") && "something" == json.get("string")
        json.has("double") && 1.25 == json.get("double")
        json.has("long") && 145 == json.get("long")
        145 == json.get("long", 200)

        when:
        int value = json.get("not there")

        then:
        thrown(Exception)

        when:
        Integer valueObj = json.get("not there")

        then:
        null == valueObj

        when:
        value = json.get("not there", 25)

        then:
        25 == value
    }

    def 'test child object'() {
        given:
        def json = new JsonObject()

        when:
        def child = json.newChildObject("")

        then:
        child == null

        when:
        child = json.newChildObject(null)

        then:
        child == null

        when:
        child = json.newChildObject("child")
        def fetchedChild = json.get("child")

        then:
        child != json
        fetchedChild.equals(child)
    }

    def 'test child array'() {
        given:
        def json = new JsonObject()

        when:
        def array = json.newChildArray(" ")

        then:
        array == null

        when:
        array = json.newChildArray(null)

        then:
        array == null

        when:
        array = json.newChildArray("child")
        def child = json.get("child")

        then:
        array == child
    }

    def 'test LocalDate'() {
        given:
        def json = new JsonObject()

        when:
        LocalDate now = LocalDate.now()
        json.put("date", now)
        LocalDate fetched = json.getDate("date")

        then:
        fetched != null
        fetched == now

        when:
        String asString = json.get("date")

        then:
        asString instanceof String
        isNotBlank(asString)

        when:
        json.put("nullDate", (LocalDate)null)

        then:
        null == json.getDate("nullDate")

        when:
        json.put("dateAsString", "2019-08-21")
        fetched = json.getDate("dateAsString")

        then:
        null != fetched
        fetched.getYear() == 2019
        fetched.getMonth() == Month.AUGUST
        fetched.getDayOfMonth() == 21
    }

    def 'test LocalDateTime'() {
        given:
        def json = new JsonObject()

        when:
        LocalDateTime now = LocalDateTime.now()
        println now.format(DateTimeFormatter.ISO_DATE_TIME)
        json.put("date", now)
        LocalDateTime fetched = json.getDateTime("date")

        then:
        fetched != null
        fetched == now

        when:
        String asString = json.get("date")

        then:
        asString instanceof String
        isNotBlank(asString)

        when:
        json.put("nullDate", (LocalDateTime)null)

        then:
        null == json.getDate("nullDate")

        when:
        json.put("dateAsString", "2019-08-21T11:43:39.000")
        fetched = json.getDateTime("dateAsString")
        println fetched

        then:
        null != fetched
        fetched.getYear() == 2019
        fetched.getMonth() == Month.AUGUST
        fetched.getDayOfMonth() == 21
        fetched.getHour() == 11
        fetched.getMinute() == 43
        fetched.getSecond() == 39
    }
}
